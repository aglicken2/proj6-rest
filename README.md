# Project 6: Brevet time calculator service

Simple listing service from project 5 stored in MongoDB database. Completed by Anne Glickenhaus aglicken@uoregon.edu. Whiteboard logic worked on with Alyssa Kelley.

## What is in this repository

This program is a basic REST API to retrieve items stored in a database; it is bassed on our previous progject of calulating brevet open and close times and storing those controls, and open/close times in a Mongo database.  

To run this program, run the command " docker-compose up --build " in the terminal. When the build is complete, the calculations for brevet times occur at localhost:5010, follow http://localhost:5010 to submit brevet control distances (in whole numbers of miles). Once controls have been submitted, follow http://localhost:5000 to see all functionalities added below. To see each functionality in an individual page follow http://localhost:5001 followed by the desired functionality (eg. " /listAll ", " /listOpenOnly/csv ", "listCloseOnly/json?top=5", etc.).

note: listAll are all presented in the order input into the database. 
The top item option is for OpenOnly and CloseOnly times.
All OpenOnly and CloseOnly are displayed in sorted ascending order.


## Functionality added

This project has following four parts.

* a RESTful service exposes what is stored in MongoDB. Specifically, the following three basic APIs:
    * "http://localhost:5001/listAll" returns all open and close times in the database
    * "http://localhost:5001/listOpenOnly" returns open times only
    * "http://localhost:5001/listCloseOnly" returns close times only

* Two different representations: one in csv and one in json. For the above, JSON is the default representation for the above three basic APIs. 
    * "http://localhost:5001/listAll/csv" returns all open and close times in CSV format
    * "http://localhost:5001/listOpenOnly/csv" returns open times only in CSV format
    * "http://localhost:5001/listCloseOnly/csv" returns close times only in CSV format

    * "http://localhost:5001/listAll/json" returns all open and close times in JSON format
    * "http://localhost:5001/listOpenOnly/json" returns open times only in JSON format
    * "http://localhost:5001/listCloseOnly/json" returns close times only in JSON format

* You will also add a query parameter to get top "k" open and close times. For examples, see below.

    * "http://localhost:5001/listOpenOnly/csv?top=3" returns top 3 open times only (in ascending order) in CSV format 
    * "http://localhost:5001/listOpenOnly/json?top=5" returns top 5 open times only (in ascending order) in JSON format
    * "http://localhost:5001/listCloseOnly/csv?top=6" returns top 5 close times only (in ascending order) in CSV format
    * "http://localhost:5001/listCloseOnly/json?top=4" returns top 4 close times only (in ascending order) in JSON format

* A consumer program uses the services created. Using php, "website" inside DockerRestAPI shows each of the above APIs in their own page. 
