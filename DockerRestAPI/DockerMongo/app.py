import os
from flask import Flask, redirect, url_for, request, render_template, jsonify
from pymongo import MongoClient

import arrow
import acp_times

import config

import logging   # Better than print statements
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.INFO)
log = logging.getLogger(__name__)

app = Flask(__name__)

client = MongoClient('db', 27017)
db = client.tododb
db.tododb.delete_many({})

###
# Globals
###
app = Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    session['linkback'] = url_for("index")
    return render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    distance = request.args.get("totdistance", 0, type=int) #getting the full distance of the brevet    
    
    startdate = request.args.get('startdate', "", type=str)
    starttime = request.args.get('starttime', "", type=str)
    
    full_time = arrow.get(startdate + " " + starttime).isoformat()    
    
    open_time = acp_times.open_time(km, distance, full_time)
    close_time = acp_times.close_time(km, distance, full_time)
    
    result = {"open": open_time, "close": close_time}
    return jsonify(result=result)

@app.route('/display', methods=['POST'])
def todo():
    _items = db.tododb.find()
    items = [item for item in _items]
    
    if(len(items) == 0):
        return render_template('display_error.html')

    return render_template('display.html', items=items)

@app.route('/new', methods=['POST'])
def new():
    # item_doc = {
    #     'name': request.form['name'],
    #     'description': request.form['description']
    # }
    # db.tododb.insert_one(item_doc)
    
    km_dist_list = request.form.getlist("km")
    print("km distance list", km_dist_list)
    open_time_list = request.form.getlist("open")
    print("open time list",open_time_list)
    close_time_list = request.form.getlist("close")
    print("close time list",close_time_list)

    checkpoints = []
    open_times = []
    close_times = []
    
    for i in km_dist_list:
        if(str(i) != ""): 
            # if the string is not empty, append the item to the checkpoint
            checkpoints.append(str(i))
    
    for i in open_time_list:
        if(str(i) != ""): 
            # if the string is not empty, append the item to the open times
            open_times.append(str(i))
            
    for i in close_time_list:
        if(str(i) != ""): 
            # if the string is not empty, append the item to the close times
            close_times.append(str(i))
         
    if((len(checkpoints) == 0) or (len(open_times) == 0) or (len(close_times) == 0)):
        return render_template('no_item_error.html')
       
    
    for i in range(len(checkpoints)):
        item_doc = {
            'Checkpoint': checkpoints[i],
            'Open_Times': open_times[i],
            'Close_Times': close_times[i]
        }
        db.tododb.insert_one(item_doc)

    return redirect(url_for('index'))

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
