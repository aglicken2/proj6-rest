<html>
    <head>
        <title>CIS 322 REST-api demo: Laptop list</title>
    </head>

    <body>
        <!-- <h1>List of laptops</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/');
            $obj = json_decode($json);
	          $laptops = $obj->Laptops;
            foreach ($laptops as $l) {
                echo "<li>$l</li>";
            }
            ?> -->
			<ul>
	        <h1>List All</h1>
	            <?php
	            $json = file_get_contents('http://laptop-service/listAll');
	            $obj = json_decode($json);
				
		          $open_time = $obj->Opening_Times;
				echo "Open Times:\n\n";
	            foreach ($open_time as $l) {
	                echo "<li>$l</li>";
	            }
		
				  $close_time = $obj->Closing_Times;
				echo "Close Times:\n\n";
	            foreach ($close_time as $l) {
	                echo "<li>$l</li>";
	            }
	            ?>
				
			<h1>List Open Only</h1>
				
	            <?php
	            $json = file_get_contents('http://laptop-service/listOpenOnly');
	            $obj = json_decode($json);
				
		          $open_time = $obj->Opening_Times;
				echo "Open Times:\n\n";
	            foreach ($open_time as $l) {
	                echo "<li>$l</li>";
	            }

	            ?>
				
			<h1>List Close Only</h1>
				
	            <?php
	            $json = file_get_contents('http://laptop-service/listCloseOnly');
	            $obj = json_decode($json);
		
				  $close_time = $obj->Closing_Times;
				echo "Close Times:\n\n";
	            foreach ($close_time as $l) {
	                echo "<li>$l</li>";
	            }
	            ?>
				
	      <h1>List All CSV</h1>
		  	<?php
			echo file_get_contents('http://laptop-service/listAll/csv');
			?>
			
		  <h1>List Open Only CSV</h1>
		  	<?php
		  	echo file_get_contents('http://laptop-service/listOpenOnly/csv');
		  	?>
				
	      <h1>List Close Only CSV</h1>
		  	<?php
		  	echo file_get_contents('http://laptop-service/listCloseOnly/csv');
		  	?>
				
			
				
		     <h1>List All Json</h1>
		         <?php
		          $json = file_get_contents('http://laptop-service/listAll/json');
		          $obj = json_decode($json);
				
			        $open_time = $obj->Opening_Times;
				  echo "Open Times:\n\n";
		          foreach ($open_time as $l) {
					  echo "<li>$l</li>";
		            }
		
					  $close_time = $obj->Closing_Times;
				   echo "Close Times:\n\n";
		           foreach ($close_time as $l) {
					   echo "<li>$l</li>";
		            }
		            ?>
					
			     <h1>List Open Only Json</h1>
			            <?php
			            $json = file_get_contents('http://laptop-service/listOpenOnly/json');
			            $obj = json_decode($json);
				
				          $open_time = $obj->Opening_Times;
						echo "Open Times:\n\n";
			            foreach ($open_time as $l) {
			                echo "<li>$l</li>";
			            }
						?>
						
		        <h1>List Close Only Json</h1>
		            <?php
		            $json = file_get_contents('http://laptop-service/listCloseOnly/json');
		            $obj = json_decode($json);
				
					  $close_time = $obj->Closing_Times;
					echo "Close Times:\n\n";
		            foreach ($close_time as $l) {
		                echo "<li>$l</li>";
		            }
		            ?>

        </ul>
    </body>
</html>
