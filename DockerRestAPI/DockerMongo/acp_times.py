"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
import logging

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    # log.info("Conrrol dist is: {}\n".format(control_dist_km))
    # log.info("Brevet dist is: {}\n".format(brevet_dist_km))
    # log.info("Brevet start time is: {}\n".format(drevet_start_time))
    
    #the opening time is how long it would take the ave max speed to arrive 
    
    # if(control_dist_km > brevet_dist_km):
    #     control_dist_km = brevet_dist_km
    
    open_time = 0
    control_dist = control_dist_km
    
    while(0 < control_dist):
    
        if(0 < control_dist <=200):            
            open_time += (control_dist/34)
            
            # temp_dist = control_dist - control_dist
            control_dist = 0
    
        elif(200 < control_dist <=400):
            temp_time = control_dist - 200
            open_time += (temp_time/32)
        
            temp_dist = control_dist - temp_time
            control_dist = temp_dist
    
        elif(400 < control_dist <=600):            
            temp_time = control_dist - 400
            open_time += (temp_time/30)
            
            temp_dist = control_dist - temp_time
            control_dist = temp_dist
    
        elif(600 < control_dist <=1000):
            temp_time = control_dist - 600
            open_time += (temp_time/28)
            
            temp_dist = control_dist - temp_time
            control_dist = temp_dist
        
    #comvert opening time into minutes
    open_time_minutes = round((open_time*60 + .5),0)
    
    return arrow.get(brevet_start_time).shift(minutes=open_time_minutes).isoformat()
    
    # return arrow.now().isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
  
    # if(control_dist_km > brevet_dist_km):
    #     control_dist_km = brevet_dist_km
    
    close_time = 0
    control_dist = control_dist_km
    
    print("The starting control distance is: ", control_dist)
    
    while(0 < control_dist):    
    
        if(0 < control_dist <= 600):
            close_time += (control_dist/15)
            
            # temp_dist = control_dist - control_dist_km
            control_dist = 0

        elif(600 < control_dist <= 1000):
            temp_time = control_dist_km - 600
            close_time += (temp_time/11.428)
            
            temp_dist = control_dist - temp_time
            control_dist = temp_dist
            
        
    close_time_minutes = round((close_time*60 + .5), 0)
    
    return arrow.get(brevet_start_time).shift(minutes=close_time_minutes).isoformat()
    # return arrow.now().isoformat()
    
    
if __name__ == "__main__":
    start_time = arrow.Arrow(2020,1,1)
    brevet_dist_km = 600
    control = 550

    print("Starting time at", start_time)
    print("closing times for 600km brevet with control at 550", open_time(control, brevet_dist_km, arrow.get(start_time)))

    
    
