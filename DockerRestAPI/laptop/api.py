# Laptop Service

import os
import flask
from flask import Flask, redirect, url_for, request, render_template, jsonify
import pymongo
from pymongo import MongoClient

from flask_restful import Resource, Api

import logging   # Better than print statements
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.INFO)
log = logging.getLogger(__name__)

# Instantiate the app
app = Flask(__name__)
api = Api(app)

client = MongoClient('db', 27017)
db = client.tododb

# class Laptop(Resource):
#     def get(self):
#         return {
#             'Laptops': ['Mac OS', 'Dell',
#             'Windozzee',
#         'Yet another laptop!',
#         'Yet yet another laptop!'
#             ]
#         }
        
class listAll(Resource):
    def get(self):
        
        _items = db.tododb.find() 
        
        items = [item for item in _items]
        
        return{
            'Opening_Times': [item['Open_Times'] for item in items],
            'Closing_Times': [item['Close_Times'] for item in items]
        }


class listOpen(Resource):
    def get(self):
        top_value = request.args.get("top")
        # if (top_value != None):
        #     _items = db.tododb.find().sort('Open_Times', pymongo.ASCENDING).limit(int(top_value))
        #     items = [item for item in _items]
        #     return{
        #         'Opening_Times': [item['Open_Times'] for item in items]
        #     }
        if (top_value == None):
            top_value = 25 
        
        _items = db.tododb.find().sort('Open_Times', pymongo.ASCENDING).limit(int(top_value))
        
        items = [item for item in _items]
        
        return{
            'Opening_Times': [item['Open_Times'] for item in items]
        }


class listClose(Resource):
    def get(self):
        top_value = request.args.get("top")
        
        # if (top_value != None):
        #     _items = db.tododb.find().sort('Close_Times', pymongo.ASCENDING).limit(int(top_value))
        #     items = [item for item in _items]
        #     return{
        #         'Closing_Times': [item['Close_Times'] for item in items]
        #     }
        
        if (top_value == None):
            top_value = 25
        
        _items = db.tododb.find().sort('Close_Times', pymongo.ASCENDING).limit(int(top_value))
        
        items = [item for item in _items]
        
        return{
            'Closing_Times': [item['Close_Times'] for item in items]
        }
        
    
class allCSV(Resource):
    def get(self):        
        _items = db.tododb.find()
        
        items = [item for item in _items]
        
        csv_file = ""
        
        for item in items:
            csv_file += item['Open_Times'] + "," + item['Close_Times']
            
            if item != (len(items) -1):
                csv_file += ","
        
        return csv_file
    
class openCSV(Resource):
    def get(self):
        
        csv_file = ""
        
        top_value = request.args.get("top")
        # if (top_value != None):
        #     _items = db.tododb.find().sort('Open_Times', pymongo.ASCENDING).limit(int(top_value))
        #     items = [item for item in _items]
        #
        #     for i in items:
        #         csv_file += item['Open_Times']
        #
        #         if i != (len(items) -1):
        #             csv_file += ","
        #
        #     return csv_file
        if (top_value == None):
            top_value = 25
            
        
        _items = db.tododb.find().sort('Open_Times', pymongo.ASCENDING).limit(int(top_value))
        
        items = [item for item in _items]
        
        for item in items:
            csv_file += item['Open_Times']
            
            if item != (len(items) -1):
                csv_file += ","
        
        return csv_file
    

class closeCSV(Resource):
    def get(self):
        
        csv_file = ""
        
        top_value = request.args.get("top")
        # if (top_value != None):
        #     _items = db.tododb.find().sort('Close_Times', pymongo.ASCENDING).limit(int(top_value))
        #     items = [item for item in _items]
        #
        #     for i in items:
        #         csv_file += item['Close_Times']
        #
        #         if i != (len(items) -1):
        #             csv_file += ","
        #
        #     return csv_file
        if (top_value == None):
            top_value = 25
        
        _items = db.tododb.find().sort('Close_Times', pymongo.ASCENDING).limit(int(top_value))
        
        items = [item for item in _items]
        
        for item in items:
            csv_file += item['Close_Times']
            
            if item != (len(items) -2):
                csv_file += ","
        
        return csv_file
    
class allJson(Resource):
    def get(self):
        _items = db.tododb.find()
        
        items = [item for item in _items]
        
        return{
            'Opening_Times': [item['Open_Times'] for item in items],
            'Closing_Times': [item['Close_Times'] for item in items]
        }
    
class openJson(Resource):
    def get(self):
        top_value = request.args.get("top")
        # if (top_value != None):
        #     _items = db.tododb.find().sort('Open_Times', pymongo.ASCENDING).limit(int(top_value))
        #     items = [item for item in _items]
        #     return{
        #         'Opening_Times': [item['Open_Times'] for item in items]
        #     }
        
        if (top_value == None):
            top_value = 25
        
        _items = db.tododb.find().sort('Open_Times', pymongo.ASCENDING).limit(int(top_value))
        
        items = [item for item in _items]
        
        return{
            'Opening_Times': [item['Open_Times'] for item in items]
        }

class closeJson(Resource):
    def get(self):
        top_value = request.args.get("top")
        # if (top_value != None):
        #     _items = db.tododb.find().sort('Close_Times', pymongo.ASCENDING).limit(int(top_value))
        #     items = [item for item in _items]
        #     return{
        #         'Closing_Times': [item['Close_Times'] for item in items]
        #     }
        
        if (top_value == None):
            top_value = 25
        
        _items = db.tododb.find().sort('Close_Times', pymongo.ASCENDING).limit(int(top_value))
        
        items = [item for item in _items]
        
        return{
            'Closing_Times': [item['Close_Times'] for item in items]
        }

# Create routes
# Another way, without decorators
# api.add_resource(Laptop, '/')

api.add_resource(listAll, '/listAll')
api.add_resource(listOpen, '/listOpenOnly')
api.add_resource(listClose, '/listCloseOnly')

api.add_resource(allCSV, '/listAll/csv')
api.add_resource(openCSV, '/listOpenOnly/csv')
api.add_resource(closeCSV, '/listCloseOnly/csv')

api.add_resource(allJson, '/listAll/json')
api.add_resource(openJson, '/listOpenOnly/json')
api.add_resource(closeJson, '/listCloseOnly/json')

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
